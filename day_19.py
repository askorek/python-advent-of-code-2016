all_elves = 3004953
elves = [i for i in range(1, all_elves+1)]

prev_last_val = None
while len(elves) > 1:
    last_val = elves[-1]
    if prev_last_val in elves:
        elves = elves[1::2]
    else:
        elves = elves[0::2]
    prev_last_val = last_val
print(elves)

# -------------------- part 2 ------------------------ #
all_elves = 3004953
elves = [i for i in range(1, all_elves + 1)]
current_index = 0

def find_next(last_index, lenn, steal_index):
    if steal_index > last_index:
        return last_index + 1
    else:
        return last_index%lenn


def find_steal(next_elve, lenn):
    return (next_elve + lenn // 2) % lenn

lenn = all_elves
while lenn > 1:
    if lenn % 1000 == 0: print(lenn)
    steal_from_this = find_steal(current_index, lenn)
    del elves[steal_from_this]

    last_index = current_index
    lenn -= 1
    current_index = find_next(last_index, lenn, steal_from_this)
print(elves)
