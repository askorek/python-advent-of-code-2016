# def check_if_map_is_generated_properly():
#
#     for i in range(7):
#         line = ''
#         for j in range(10):
#             if is_open(j, i, 1352):
#                 line += '.'
#             else:
#                 line += '#'
#         print(line)
#
#path_lengths = []
#
# def find_path(current, goal, history):
#     global path_lengths
#
#
#     if current == goal:
#         new_history = history[:] + [current]
#         #print(new_history)
#         #print(len(new_history))
#         path_lengths.append(len(new_history))
#         return new_history
#
#
#     ]
#
#     for neigh in list(neighbours):
#         if neigh[0] < 0 or neigh[1] < 0 or neigh in history:
#             neighbours.remove(neigh)
#
#     new_history = history[:] + [current]
#
#     for neigh in neighbours:
#         if is_open(neigh[0], neigh[1], favourite_num):
#             find_path(neigh, goal, new_history)
#
#
# find_path(start_point, finish_point, [])
# print(min(path_lengths))

#----------------- after reorganization ---------------------------

def is_open(x,y, fav):
    assert x >= 0 and y >= 0
    num = x*x + 3*x + 2*x*y + y + y*y + int(fav)
    ones = bin(num).count('1')
    return ones % 2 == 0

def get_neighbours(current):
    global visited
    neighbours = [
        (current[0] - 1, current[1], current[2] + 1),
        (current[0] + 1, current[1], current[2] + 1),
        (current[0], current[1] - 1, current[2] + 1),
        (current[0], current[1] + 1, current[2] + 1)]

    # check if in range and if not visited previously
    for neigh in list(neighbours):
        if neigh[0] < 0 or neigh[1] < 0 or (neigh[0], neigh[1]) in visited:
            neighbours.remove(neigh)
    # check if open
    for neigh in list(neighbours):
        if not is_open(neigh[0], neigh[1], favourite_num):
            neighbours.remove(neigh)

    return neighbours

# --- part 1 ----
finish_point = (31,39) #(7,4)
visited = {}
to_visit = [(1,1,0)]
favourite_num = 1352

while len(to_visit) > 0:
    current = to_visit.pop(0)
    if current[0] == finish_point[0] and current[1] == finish_point[1]:
        print("Fount at {}".format(current[2]))
        break

    to_visit += get_neighbours(current)
    visited[(current[0], current[1])] = current[2]


# --- part 2 ----

how_many = 0
for place in visited:
    if visited[place] <= 50:
        how_many += 1
print(how_many)

