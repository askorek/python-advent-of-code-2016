import cProfile

def dragon_step(input_string):
    a = input_string
    b = input_string[::-1].replace('0','a').replace('1', '0').replace('a','1')
    return ''.join([a, '0', b])

def fill_disk(input_string, size):
    while len(input_string) < size:
        input_string = dragon_step(input_string)
    return input_string[:size]

def calculate_checksum(data):

    def make_half(data_to_half):
        assert len(data_to_half) % 2 == 0
        output = []
        for i in range(len(data_to_half)//2):
            if data_to_half[2*i] == data_to_half[2*i+1]:
                output.append('1')
            else:
                output.append('0')
        return ''.join(output)

    while len(data)%2 == 0:
        data = make_half(data)

    return data

def main():
    start = '10001110011110000'
    longer = fill_disk(start, 35651584)
    print(calculate_checksum(longer))

# cProfile.run('main()')