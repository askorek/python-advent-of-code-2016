import itertools
import cProfile

with open("day_24.txt") as f:
    data = f.read().splitlines()

nx = len(data[0])
ny = len(data)

# find numbers position
numbers_positions = {}
for y in range(ny):
    for x in range(nx):
        if data[y][x] not in '.#':
            numbers_positions[data[y][x]] = (y,x)

def get_neighbours(current, data, visited):
    neighbours = [
        (current[0] - 1, current[1], current[2] + 1),
        (current[0] + 1, current[1], current[2] + 1),
        (current[0], current[1] - 1, current[2] + 1),
        (current[0], current[1] + 1, current[2] + 1)]

    delete_closed(data, neighbours)

    delete_visited(neighbours, visited)

    for neigh in neighbours:
        visited[neigh[0], neigh[1]] = neigh[2]
    return neighbours


def delete_visited(neighbours, visited):
    # check if in range and if not visited previously
    for neigh in list(neighbours):
        if (neigh[0], neigh[1]) in visited:
            neighbours.remove(neigh)


def delete_closed(data, neighbours):
    # check if open
    for neigh in list(neighbours):
        if data[int(neigh[0])][int(neigh[1])] == '#':
            neighbours.remove(neigh)


def find_distance(point_1, point_2, data):
    finish_point = (point_2[0], point_2[1])
    visited = {}
    to_visit = [(point_1[0], point_1[1], 0)]

    while len(to_visit) > 0:
        current = to_visit.pop(0)
        if current[0] == finish_point[0] and current[1] == finish_point[1]:
            return current[2]
        new_neighbours = get_neighbours(current, data, visited)
        to_visit += new_neighbours
        visited[(current[0], current[1])] = current[2]
distances = {}
for el in itertools.combinations('01234567', 2):
    distances[el] = find_distance(numbers_positions[el[0]], numbers_positions[el[1]], data)

print(distances)

lowest = 9999999999999999999
for el in itertools.permutations('1234567'):
    el = ['0'] + list(el)
    total_path = 0
    for i in range(len(el) - 1):
        dist = distances.get((el[i], el[i+1]))
        if dist is None:
            dist = distances.get((el[i+1], el[i]))
        total_path += dist
    if total_path < lowest:
        lowest = total_path

print(lowest)
#cProfile.run("find_distance(numbers_positions['0'], numbers_positions['4'], data)")

# ------------- part 2 ------------------
lowest = 9999999999999999999
for el in itertools.permutations('1234567'):
    el = ['0'] + list(el) + ['0']
    total_path = 0
    for i in range(len(el) - 1):
        dist = distances.get((el[i], el[i+1]))
        if dist is None:
            dist = distances.get((el[i+1], el[i]))
        total_path += dist
    if total_path < lowest:
        lowest = total_path

print(lowest)
