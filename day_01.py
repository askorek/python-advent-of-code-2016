input = "R3, L5, R2, L2, R1, L3, R1, R3, L4, R3, L1, L1, R1, L3, R2, L3, L2, R1, R1, L1, R4, L1, L4, R3, L2, L2, R1, L1, R5, R4, R2, L5, L2, R5, R5, L2, R3, R1, R1, L3, R1, L4, L4, L190, L5, L2, R4, L5, R4, R5, L4, R1, R2, L5, R50, L2, R1, R73, R1, L2, R191, R2, L4, R1, L5, L5, R5, L3, L5, L4, R4, R5, L4, R4, R4, R5, L2, L5, R3, L4, L4, L5, R2, R2, R2, R4, L3, R4, R5, L3, R5, L2, R3, L1, R2, R2, L3, L1, R5, L3, L5, R2, R4, R1, L1, L5, R3, R2, L3, L4, L5, L1, R3, L5, L2, R2, L3, L4, L1, R1, R4, R2, R2, R4, R2, R2, L3, L3, L4, R4, L4, L4, R1, L4, L4, R1, L2, R5, R2, R3, R3, L2, L5, R3, L3, R5, L2, R3, R2, L4, L3, L1, R2, L2, L3, L5, R3, L1, L3, L4, L3"
# input = "R5, L5, R5, R3"
tab_input = input.split(", ")

sign = lambda a: (a>0) - (a<0)

grid_directions = [0,1],[1,0],[0,-1],[-1,0] # NESW
grid_directions_pointer = 0

position = [0,0]  # x,y
visited_set = set()
again_in_the_same_place = False

def make_move(position, move):
    global visited_set, again_in_the_same_place, grid_directions_pointer

    if move[0] == 'R':
        grid_directions_pointer = (grid_directions_pointer + 1) % 4
    else:
        grid_directions_pointer = (grid_directions_pointer - 1) % 4

    delta_x = grid_directions[grid_directions_pointer][0] * int(move[1:])
    delta_y = grid_directions[grid_directions_pointer][1] * int(move[1:])

    # moving one unit at time and marking all as visited
    for dx in range(abs(delta_x)):
        position[0] += sign(delta_x)
        if tuple(position) not in visited_set and not again_in_the_same_place:
            visited_set.add(tuple(position))
        elif not again_in_the_same_place:
            print(abs(position[0])+abs(position[1]))
            again_in_the_same_place = True
    for dy in range(abs(delta_y)):
        position[1] += sign(delta_y)
        if tuple(position) not in visited_set and not again_in_the_same_place:
            visited_set.add(tuple(position))
        elif not again_in_the_same_place:
            print(abs(position[0])+abs(position[1]))
            again_in_the_same_place = True
    return position


print(visited_set)
for el in tab_input:
    position = make_move(position, el)
print(abs(position[0])+abs(position[1]))