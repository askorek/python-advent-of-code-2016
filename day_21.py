from itertools import permutations
from cProfile import Profile

my_string = 'abcdefgh'
my_string = list(my_string)

with open("day_21.txt") as f:
    data = f.read().splitlines()

def swap_position(position1, position2, string_to_swap):
    temp = string_to_swap[position1]
    string_to_swap[position1] = string_to_swap[position2]
    string_to_swap[position2] = temp
    return string_to_swap

def swap_letter(letter1, letter2, string_to_swap):
    position1 = string_to_swap.index(letter1)
    position2 = string_to_swap.index(letter2)
    return swap_position(position1, position2, string_to_swap)

def reverse(position1, position2, string_to_reverse):
    cpy = string_to_reverse[position1:position2+1:]
    cpy.reverse()
    string_to_reverse[position1:position2+1] = cpy
    return string_to_reverse

def rotate(position, list_to_rotate):
    temp = list_to_rotate[position%len(list_to_rotate):] + list_to_rotate[:position%len(list_to_rotate)]
    list_to_rotate = temp
    return list_to_rotate

def move(position1, postion2, string_to_move):
    temp = string_to_move[position1]
    del string_to_move[position1]
    new_string = string_to_move[:postion2] + [temp] + string_to_move[postion2:]
    string_to_move = new_string
    return string_to_move

def rotate_based_on_letter(letter, list_to_rotate):
    index = list_to_rotate.index(letter)
    if index >= 4:
        index += 1
    return rotate(-index-1, list_to_rotate)

def scramble_string(scrambled_string, data):
    for line in data:
        splitted = line.split()
        if splitted[0] == 'swap' and splitted[1] == "position":
            scrambled_string = swap_position(int(splitted[2]), int(splitted[5]), scrambled_string)
        elif splitted[0] == 'swap' and splitted[1] == 'letter':
            scrambled_string = swap_letter(splitted[2], splitted[5], scrambled_string)
        elif splitted[0] == 'reverse':
            scrambled_string = reverse(int(splitted[2]), int(splitted[4]), scrambled_string)
        elif splitted[0] == 'rotate' and (splitted[3] == 'step' or splitted[3] == 'steps'):
            steps = int(splitted[2])
            if splitted[1] == 'right':
                steps = steps*(-1)
            scrambled_string = rotate(steps, scrambled_string)
        elif splitted[0] =='move':
            scrambled_string = move(int(splitted[2]), int(splitted[5]), scrambled_string)
        elif splitted[0] == 'rotate' and splitted[1] == 'based':
            scrambled_string = rotate_based_on_letter(splitted[-1], scrambled_string)
        else:
            print("some error with line: {}".format(line))
            raise Exception
    return ''.join(scrambled_string)

print("part 1 result: {}".format(scramble_string(my_string[:], data)))
# -------------------------------- PART 2 -------------------

my_string = 'abcdefgh'
my_string = list(my_string)
desired = 'fbgdceah'

for permut in permutations(my_string):
    if scramble_string(list(permut), data) == 'fbgdceah':
        print("Success of part2. Result found at: {}".format("".join(permut)))
        break