import hashlib
passcode = 'pvhmgsws'

visited = {}
to_visit = [(0,0,"")]
finish_point = (3,3)


def is_open(inp):
    m = hashlib.md5()
    validation_string = (passcode + inp[:-1]).encode('utf-8')
    m.update(validation_string)

    if inp[-1] == 'U' and m.hexdigest()[0] in 'bcdef' or\
        inp[-1] == 'D' and m.hexdigest()[1] in 'bcdef' or\
        inp[-1] == 'L' and m.hexdigest()[2] in 'bcdef' or\
        inp[-1] == 'R' and m.hexdigest()[3] in 'bcdef':
        return True
    return False

def get_neighbours(current):
    neighbours = [
        (current[0] - 1, current[1], current[2] + 'L'),
        (current[0] + 1, current[1], current[2] + 'R'),
        (current[0], current[1] - 1, current[2] + 'U'),
        (current[0], current[1] + 1, current[2] + 'D')]

    # check if in range
    for neigh in list(neighbours):
        if neigh[0] < 0 or neigh[1] < 0 or neigh[0] > 3 or neigh[1] > 3:
            neighbours.remove(neigh)

    # check if open:
    for neigh in list(neighbours):
        if not is_open(neigh[2]):
            neighbours.remove(neigh)

    return neighbours


while len(to_visit) > 0:
    current = to_visit.pop(0)
    if current[0] == finish_point[0] and current[1] == finish_point[1]:
        print("Fount at {}".format(current[2]))
        print("path length was {}".format(len(current[2])))
        break

    to_visit += get_neighbours(current)
    visited[(current[0], current[1])] = current[2]

# ----- part 2 -------
visited = {}
to_visit = [(0,0,"")]
finish_point = (3,3)
finished = []


while len(to_visit) > 0:
    current = to_visit.pop(0)
    if current[0] == finish_point[0] and current[1] == finish_point[1]:
        finished.append(len(current[2]))
        continue
    to_visit += get_neighbours(current)
    visited[(current[0], current[1])] = current[2]
print("max length = {}".format(max(finished)))