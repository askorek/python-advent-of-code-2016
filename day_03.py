with open("day_03.txt") as f:
    data = f.readlines()

proper_triangles = 0
for line in data:
    numbers = line.split()
    numbers = [int(a) for a in numbers]
    numbers.sort()
    if numbers[0] + numbers[1] > numbers[2]:
        proper_triangles += 1

print(proper_triangles)


# ------------- part 2 ---------------
row_1 = []
row_2 = []
row_3 = []
proper_triangles = 0

for i, line in enumerate(data):
    if i%3 == 0:
        row_1 = line[:].split()
    elif i%3 == 1:
        row_2 = line[:].split()
    else :
        row_3 = line[:].split()

        row_1 = [int(a) for a in row_1]
        row_2 = [int(a) for a in row_2]
        row_3 = [int(a) for a in row_3]

        tr_1 = sorted([row_1[0], row_2[0], row_3[0]])
        tr_2 = sorted([row_1[1], row_2[1], row_3[1]])
        tr_3 = sorted([row_1[2], row_2[2], row_3[2]])

        if tr_1[0] + tr_1[1] > tr_1[2]:
            proper_triangles += 1
        if tr_2[0] + tr_2[1] > tr_2[2]:
            proper_triangles += 1
        if tr_3[0] + tr_3[1] > tr_3[2]:
            proper_triangles += 1

print(proper_triangles)
