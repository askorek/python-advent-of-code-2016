with open("day_06.txt") as f:
    data = f.read().splitlines()

list_of_dicts = [{} for _ in range(8)]

for line in data:
    for i, letter in enumerate(line):
        if not letter in list_of_dicts[i]:
            list_of_dicts[i][letter] = 1
        else:
            list_of_dicts[i][letter] += 1

for dictionary in list_of_dicts:
    sorted_d = [v[0] for v in sorted(dictionary.items(), key=lambda kv: (-kv[1], kv[0]))]
    # print(sorted_d[0]) # - part 1
    print(sorted_d[-1])  # - part 2