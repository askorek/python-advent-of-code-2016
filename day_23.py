registers = {'a':12, 'b':0, 'c':0, 'd':0}

with open("day_23.txt") as f:
    data = f.read().splitlines()

# data = """cpy 2 a
# tgl a
# tgl a
# tgl a
# cpy 1 a
# dec a
# dec a""".splitlines()


def is_integer(value):
    try:
        int(value)
        return True
    except ValueError:
        return False

def cpy(val_reg, reg):
    global registers
    if is_integer(reg):
        pass
    if not is_integer(val_reg):
        registers[reg] = registers[val_reg]
    else:
        registers[reg] = int(val_reg)

def inc(reg):
    global registers
    if is_integer(reg):
        pass
    registers[reg] += 1

def dec(reg):
    global registers
    if is_integer(reg):
        pass
    registers[reg] -= 1

i = 0
j = 0
while i < len(data):
#    print("line {} registers: {}".format(i, registers))
    split = data[i].split()
    if split[0] == 'cpy':
        cpy(split[1], split[2])
    elif split[0] == 'inc':
        inc(split[1])
    elif split[0] == 'dec':
        dec(split[1])
    elif split[0] == "jnz":
        if ( is_integer(split[1]) and int(split[1]) != 0 ) or ( registers[split[1]] != 0 ):
            if is_integer(split[2]):
                delta = int(split[2])
            else:
                delta = int(registers[split[2]])
            if delta == 0:
                raise Exception
                delta = 1
            i += delta
            continue

    elif split[0] == 'tgl':
        if is_integer(split[1]):
            register_to_toggle = int(split[1])
        else:
            register_to_toggle = registers[split[1]]
        line_to_toggle = data[(i + register_to_toggle)%len(data)]

        if len(line_to_toggle.split()) == 2:
            if line_to_toggle.split()[0] == 'inc':
                data[(i + register_to_toggle) % len(data)] = 'dec ' + line_to_toggle.split()[1]
            else:
                data[(i + register_to_toggle) % len(data)] = 'inc ' + line_to_toggle.split()[1]
        elif len(line_to_toggle.split()) == 3:
            if line_to_toggle.split()[0] == 'jnz':
                data[(i + register_to_toggle) % len(data)] = data[i + register_to_toggle].replace('jnz', 'cpy')
            else:
                data[(i + register_to_toggle) % len(data)] = 'jnz ' + line_to_toggle.split()[1] + ' ' + line_to_toggle.split()[2]
        else:
            raise Exception("line longer than three entries")

    i += 1
    j += 1
    if(j%1000) == 0:
        print(registers)
        print(data)
        print(data[i])
    # if registers['a'] > 14519:
    #     print(registers)
    #     print(i)
    #     print(data)
    #

print(registers['a'])


