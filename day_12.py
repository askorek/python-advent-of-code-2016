registers = {'a':0, 'b':0, 'c':1, 'd':0}

with open("day_12.txt") as f:
    data = f.read().splitlines()

# data = """cpy 41 a
# inc a
# inc a
# dec a
# jnz a 2
# dec a""".splitlines()


def is_integer(value):
    try:
        int(value)
        return True
    except ValueError:
        return False

def cpy(val_reg, reg):
    global registers
    if not is_integer(val_reg):
        registers[reg] = registers[val_reg]
    else:
        registers[reg] = int(val_reg)

def inc(reg):
    global registers
    registers[reg] += 1

def dec(reg):
    global registers
    registers[reg] -= 1

i = 0
while i < len(data):
#    print("line {} registers: {}".format(i, registers))
    split = data[i].split()
    if split[0] == 'cpy':
        cpy(split[1], split[2])
    elif split[0] == 'inc':
        inc(split[1])
    elif split[0] == 'dec':
        dec(split[1])
    elif split[0] == "jnz":
        if ( is_integer(split[1]) and int(split[1]) != 0 ) or ( registers[split[1]] != 0 ):
            i += int(split[2])
            continue
    i += 1

print(registers['a'])


