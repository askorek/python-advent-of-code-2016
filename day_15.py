def prepare(positions, size):
    for i in range(len(positions)):
        positions[i] = (positions[i] + i) % size[i]


def find_time(positions, size):
    time = 0
    while True:
        for i in range(len(positions)):
            positions[i] = (positions[i] + 1) % size[i]

        if all(el == 0 for el in positions):
            print("Solution found for t = {}".format(time))
            return time
        time += 1

# --------------------- PART 1 ---------------------
positions = [2,7,10,2,9,0]
size = [5,13,17,3,19,7]

prepare(positions, size)
find_time(positions, size)


# --------------------- PART 2 --------------------
positions = [2,7,10,2,9,0,0]
size = [5,13,17,3,19,7,11]

prepare(positions, size)
find_time(positions, size)
