from copy import copy


class Screen(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.screen = [['.' for i in range(width)] for j in range(height)]

    def __str__(self):
        ret = ""
        for i in range(self.height):
            ret = ret + "".join(self.screen[i]) + '\n'
        return ret

    def rect(self, a, b):
        for i in range(b):
            for j in range(a):
                self.screen[i][j] = '#'

    def rotate_column(self, x, n):
        old_column = [copy(row[x]) for row in self.screen]
        for i, row in enumerate(self.screen):
            row[x] = old_column[(i-n) % self.height]

    def rotate_row(self, y, n):
        old_row = copy(self.screen[y])
        for i in range(self.width):
            self.screen[y][i] = old_row[(i - n) % self.width]

    def count_hashes(self):
        count = 0
        for row in self.screen:
            count += row.count('#')
        return count

with open("day_08.txt") as f:
    data = f.read().splitlines()

screen = Screen(50,6)

for line in data:
    splitted = line.split()
    if splitted[0] == 'rect':
        a, b = [int(x) for x in line.split()[1].split('x')]
        screen.rect(a,b)
    elif splitted[1] == 'row':
        y = int(splitted[2].split("=")[1])
        n = int(splitted[4])
        screen.rotate_row(y,n)
    elif splitted[1] == 'column':
        x = int(splitted[2].split("=")[1])
        n = int(splitted[4])
        screen.rotate_column(x,n)
    else:
        raise Exception("Data format invalid")


print(screen)
print(screen.count_hashes())