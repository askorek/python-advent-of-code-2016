from hashlib import md5
import re

def hash_string_one_time(word, number, dictionary):
    if number not in dictionary:
        m = md5()
        word_to_hash = word + str(number)
        word_to_hash = word_to_hash.encode('utf-8')
        m.update(word_to_hash)
        dictionary[i] = m.hexdigest()
    return dictionary[i]

def hash_string(word, number, dictionary):
    if number not in dictionary:
        current_word = word + str(number)
        for i in range(2017):
            m = md5()
            current_word = current_word.encode('utf-8')
            m.update(current_word)
            current_word = m.hexdigest()
        dictionary[number] = current_word
    return dictionary[number]

def find_three_consequent(data):
    found = re.findall(r'(\w)\1{2,}', data)
    if len(found) > 0:
        return found[0]

def check_five_consequent(data):
    found = re.findall(r'(\w)\1{4,}', data)
    if len(found) > 0:
        return found


def check_next_thousand(start_string, starting_num, letter):
    for i in range(starting_num + 1, starting_num + 1001):
        hashed = hash_string(start_string, i, dict_with_hashes)
        next_1000 = check_five_consequent(hashed)
        if next_1000 is None:
            continue
        elif letter in next_1000:
            return True
    return False


puzzle = 'ahsbgdzn'
results = []
i = 0
dict_with_hashes = dict()
while len(results) < 64:
    hashed = hash_string(puzzle, i, dict_with_hashes)
    three = find_three_consequent(hashed)
    if three is not None:
#        print("found 3 for i = {} letter is {}".format(i, three))
#        print("checking next {}".format(check_next_thousand(puzzle, i, three)))
        if check_next_thousand(puzzle, i, three):
            results.append(i)
#            print(len(results))
    i += 1
print("last hash found for i = {}".format(i-1))