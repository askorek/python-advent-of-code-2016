import re

with open("day_07.txt") as f:
    data = f.read().splitlines()

def is_symetric_pattern(word):
    if len(word) < 4:
        raise SyntaxError('line formated wrongly')
    for i in range(len(word)-3):
        if word[i] == word[i+3] and word[i+1] == word[i+2] and word[i] != word[i+1]:
            return True
    return False

#------------------- part 2 --------------------------
def find_all_aba(word):
    all_abas = []
    for i in range(len(word)-2):
        if word[i] == word[i+2] and word[i] != word[i+1]:
            all_abas.append(word[i:i+3])
    return all_abas

def generate_bab(aba):
    assert aba[0] == aba[2] and len(aba) == 3
    return aba[1] + aba[0] + aba[1]
#------------------ part 2 ------------------------



counter_abba = 0
counter_ssl = 0

for line in data:
    outsiders = re.findall(r'([^[\]]+)(?:$|\[)', line)
    insiders = re.findall(r'\[(.+?)\]',line)
    if any(is_symetric_pattern(out) for out in outsiders) and not any(is_symetric_pattern(ins) for ins in insiders):
        counter_abba += 1
    # ---- part 2 -----
    all_abas = set()
    for out in outsiders:
        abas = find_all_aba(out)
        for aba in abas:
            all_abas.add(aba)
    print(all_abas)
    for aba in all_abas:
        if any(generate_bab(aba) in i for i in insiders):
            counter_ssl += 1
            break

print(counter_abba)
print(counter_ssl)

