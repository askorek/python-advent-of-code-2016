from day_09 import decode_string, find_len

def test_first_case():
    data = 'ADVENT'
    assert decode_string(data) == 'ADVENT'

def test_second_case():
    data = 'A(1x5)BC'
    assert decode_string(data) == 'ABBBBBC'

def test_third_case():
    data = '(3x3)XYZ'
    assert decode_string(data) == 'XYZXYZXYZ'

def test_fourtht_case():
    data = 'A(2x2)BCD(2x2)EFG'
    assert decode_string(data) == 'ABCBCDEFEFG'

def test_ffth_case():
    data = '(6x1)(1x3)A'
    assert decode_string(data) == '(1x3)A'

def test_last_case():
    data = 'X(8x2)(3x3)ABCY'
    assert decode_string(data) == 'X(3x3)ABC(3x3)ABCY'


def test_first_case_len():
    data = 'ADVENT'
    assert find_len(data) == len('ADVENT')

def test_second_case_len():
    data = 'A(1x5)BC'
    assert find_len(data) == len('ABBBBBC')

def test_third_case_len():
    data = '(3x3)XYZ'
    assert find_len(data) == len('XYZXYZXYZ')

def test_fourtht_case_len():
    data = 'X(8x2)(3x3)ABCY'
    assert find_len(data) == len('XABCABCABCABCABCABCY')

def test_ffth_case_len():
    data = '(27x12)(20x12)(13x14)(7x10)(1x12)A'
    assert find_len(data) == 241920

def test_last_case_len():
    data = '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN'
    assert find_len(data) == 445