start = '.^..^^.^^^..^^.^...^^^^^....^.^..^^^.^.^.^^...^.^.^.^.^^.....^.^^.^.^.^.^.^.^^..^^^^^...^.....^....^..'


def find_traps(previous_row):
    new_row = []
    for i, el in enumerate(previous_row):
        if i == 0 or i == len(previous_row) - 1 : #first and last are virtual ok
            new_row.append('.')
            continue
        elif previous_row[i-1] == '^' and previous_row[i] == '^' and previous_row[i+1] == '.':
            new_row.append('^')
        elif previous_row[i-1] == '.' and previous_row[i] == '^' and previous_row[i+1] == '^':
            new_row.append('^')
        elif previous_row[i-1] == '^' and previous_row[i] == '.' and previous_row[i+1] == '.':
            new_row.append('^')
        elif previous_row[i-1] == '.' and previous_row[i] == '.' and previous_row[i+1] == '^':
            new_row.append('^')
        else:
            new_row.append('.')
    return ''.join(new_row)

new = start
total = 0
for _ in range(400000):
#    print new
    total = total + new.count('.') -2
    new = find_traps(new)
print(total)