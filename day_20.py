MAX_NUMBER = 4294967295

with open("day_20.txt") as f:
    data = f.read().splitlines()

rules = []
for line in data:
    splitted = line.split("-")
    rules.append((int(splitted[0]), int(splitted[1])))

rules = sorted(rules)

i = 0
ips_allowed = []
while i <= MAX_NUMBER:
    rule_launched = False
    for rule in rules:
        if (i >= rule[0] and i <= rule[1]):
            rule_launched = True
            i = rule[1] + 1
            break
    if rule_launched:
        continue
    else:
        ips_allowed.append(i)
        i += 1
    if i % 10000 == 0:
        print("jestesmy na: {%.2f}".format(i*100.0/MAX_NUMBER))

print(ips_allowed[0])
print(len(ips_allowed))
