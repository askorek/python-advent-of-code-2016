# with open("day_22.txt") as f:
#     data = f.read().splitlines()
#
# viable = 0
#
# for line1 in data:
#     for line2 in data:
#         if '/dev' not in line1 or '/dev' not in line2 or line1 == line2:
#             continue
#         assert line1.split()[2][-1] == 'T'
#         assert line2.split()[2][-1] == 'T'
#         used = int(line1.split()[2][:-1])
#         if used == 0:
#             continue
#         free = int(line2.split()[3][:-1])
#         if free >= used:
#             viable += 1
#
# print(viable)

from copy import deepcopy
from copy import copy
import cProfile


class Node(object):
    def __init__(self, x, y, size, used_space):
        self.size = size
        self.used_space = used_space
        self.x = x
        self.y = y
        self.is_goal_data = False

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash((self.size, self.used_space, self.x, self.y, self.is_goal_data))


    def can_data_be_moved(self, node_to_move):
        if self.used_space == 0 or self.used_space > (node_to_move.size - node_to_move.used_space):
            return False
        else:
            return True

    def move_data(self, node_to_move):
        assert self.can_data_be_moved(node_to_move)
        amount_to_move = self.used_space
        self.used_space = 0
        node_to_move.used_space += amount_to_move
        assert node_to_move.used_space <= node_to_move.size

        if self.is_goal_data:
            self.is_goal_data = False
            node_to_move.is_goal_data = True


def find_array_sizes(data):
    max_x = 0
    max_y = 0
    for line in data:
        if '/dev' not in line:
            continue
        x = int(line.split()[0].split('-')[1][1:])
        y = int(line.split()[0].split('-')[2][1:])
        if x > max_x:
            max_x = x
        if y > max_y:
            max_y = y
    return max_x, max_y

data = """Filesystem            Size  Used  Avail  Use%
/dev/grid/node-x0-y0   10T    8T     2T   80%
/dev/grid/node-x0-y1   11T    6T     5T   54%
/dev/grid/node-x0-y2   32T   28T     4T   87%
/dev/grid/node-x1-y0    9T    7T     2T   77%
/dev/grid/node-x1-y1    8T    0T     8T    0%
/dev/grid/node-x1-y2   11T    7T     4T   63%
/dev/grid/node-x2-y0   10T    6T     4T   60%
/dev/grid/node-x2-y1    9T    8T     1T   88%
/dev/grid/node-x2-y2    9T    6T     3T   66%""".splitlines()

with open("day_22.txt") as f:
    data = f.read().splitlines()


x, y = find_array_sizes(data)
print("max x = {}, max y = {}".format(x, y))

class NodesContainer(dict):
    def create_dict_with_all_nodes(self, data):
        for line in data:
            if '/dev' not in line:
                continue
            x = int(line.split()[0].split('-')[1][1:])
            y = int(line.split()[0].split('-')[2][1:])
            total = int(line.split()[1][:-1])
            used_space = int(line.split()[2][:-1])

            node = Node(x, y, total, used_space)
            self[(x, y)] = node

    def __eq__(self, other):
        for el in self:
            if self[el] != other[el]:
                return False
        return True

    def __key(self):
        return tuple(sorted(self.items()))

    def __hash__(self):
        return hash(self.__key())

    def get_next_steps(self, position_tuple):
        next = [(position_tuple[0] - 1, position_tuple[1]),
                (position_tuple[0] + 1, position_tuple[1]),
                (position_tuple[0], position_tuple[1] - 1),
                (position_tuple[0], position_tuple[1] + 1)]

        list_with_new_containers = []
        for el in next:
            if self.get(el) is not None:
                move_from = copy(self.get(position_tuple))
                move_to = copy(self.get(el))
                if move_from.can_data_be_moved(move_to):
                    cont_copy = copy(self)
                    cont_copy[move_from.x, move_from.y] = move_from
                    cont_copy[move_to.x, move_to.y] = move_to
                    cont_copy.get(position_tuple).move_data(cont_copy.get(el))
                    list_with_new_containers.append(cont_copy)
        return list_with_new_containers



cont = NodesContainer()
cont.create_dict_with_all_nodes(data)

# --- part 1 -----
numb = 0
for node1 in cont:
    for node2 in cont:
        if node1 != node2 and cont[node1].can_data_be_moved(cont[node2]):
            print("node1: ({}, {})\tnode2: ({}, {})".format(cont[node1].x, cont[node1].y, cont[node2].x, cont[node2].y))
            numb += 1
print(numb)

# ---- part 2 -----


for j in range(y+1):
    line = ''
    for i in range(x+1):
        if i == 0 and j == 0:
            line += 'o'
        elif i == x and j ==0:
            line += 'x'
        elif i == 26 and j == 12:
            line += '_'
        elif cont[(i,j)].used_space <= 90:
            line += '.'
        else:
            line += '#'
    print(line)




#
# cont[(32,0)].is_goal_data = True
#
# list_to_do = []
# for node in cont:
#     next_steps = cont.get_next_steps((cont[node].x, cont[node].y))
#     for step in next_steps:
#         list_to_do += [(step, 1)]
#
# visited_list = set()
#
# def big_fat_loop(list_to_do):
#     global visited_list
#
#     runs = 0
#     while len(list_to_do) > 0:
#         runs += 1
#         current_dict, no_steps = list_to_do.pop(0)
#         if runs%10 == 0: print("step: {}, size: {}".format(no_steps, len(list_to_do)))
#         if current_dict[(0,0)].is_goal_data:
#             print("Found at step: {}".format(no_steps))
#             print("loop runs: {}".format(runs))
#             break
#
#         for node in current_dict:
#             next_steps = current_dict.get_next_steps((current_dict[node].x, current_dict[node].y))
#             for step in next_steps:
#                 if step not in visited_list:
#                     visited_list.add(step)
#                     list_to_do += [(step, no_steps + 1)]
#
# #big_fat_loop(list_to_do)
# #cProfile.run("big_fat_loop(list_to_do)")