from hashlib import md5

puzzle_input = 'ugkcyxxp'
# puzzle_input = 'abc'


# --------------- part 1 --------------
# number = 0
# hashes_found = 0
# while True:
#     m = md5()
#     to_hash = puzzle_input + str(number)
#     m.update(bytes(to_hash, 'utf-8'))
#     if m.hexdigest()[:5] == '00000':
#         print(m.hexdigest())
# #        print(number)
#         hashes_found += 1
#     if hashes_found >= 8:
#         break
#     number += 1

# ------------- part 2 ------------
decoded = '________'
number = 0
while decoded.count('_') > 0:
    m = md5()
    to_hash = puzzle_input + str(number)
    m.update(bytes(to_hash, 'utf-8'))
    hashh = m.hexdigest()
    if hashh[:5] == '00000':
        if hashh[5] in '01234567':
            position = int(hashh[5])
            if decoded[position] is '_':
                decoded = decoded[:position] + hashh[6] + decoded[position+1:]
                print(decoded)
    number += 1