class Bot(object):
    dict_with_bots = {}  # all bots are stored here, key: bot number, value: bot instance

    def __init__(self, number):
        self.number = number
        self.values = []
        self.high = None
        self.low = None
        self.high_type = None
        self.low_type = None
        Bot.dict_with_bots[number] = self  # all bots are stored here

    def put_value(self, val):
        self.values.append(int(val))
        if len(self.values) == 2 and self.high is not None and self.low is not None:  # perform giveaway if bot has two values
            return self.perform_giveway()

    def perform_giveway(self):
        bot_high = self.high
        bot_low = self.low
        value_high = max(self.values)
        value_low = min(self.values)
        type_high = self.high_type
        type_low = self.low_type

        # for solving first part
        if 61 in self.values and 17 in self.values:
            print("ALARMA! we found right bot! This bot has number {}".format(self.number))

        # values are already remmembered, clear bot values before giveaway
        self.values = []

        if type_high != 'bot':  # if type == 'output' output do not pass value to other bot, just print
            print('sinking high value {} into {} sink'.format(value_high, bot_high))
        else:  # put high value to high bot
            if bot_high not in Bot.dict_with_bots:
                Bot(bot_high)
            Bot.dict_with_bots[bot_high].put_value(value_high)

        # for second part
        if type_low != 'bot' and bot_low in [0,1,2]:
            print('---------- sinking low value {} into {} sink'.format(value_low, bot_low))

        elif type_low == 'bot':  # put low value to low bot
            if bot_low not in Bot.dict_with_bots:
                Bot(bot_low)
            Bot.dict_with_bots[bot_low].put_value(value_low)

    def assign_partners(self, low, high, min_type, max_type):
        assert self.high == None and self.low == None
        self.high = high
        self.low = low
        self.low_type = min_type
        self.high_type = max_type
        if len(self.values) == 2:   # perform giveaway if bot has two values
            return self.perform_giveway()


with open("day_10.txt") as f:
    data = f.read().splitlines()

# wrapper - put value to bot, when no bot in dict create bot first
def put_and_create(bot, value):
    if bot not in Bot.dict_with_bots:
        Bot(bot)
    Bot.dict_with_bots[bot].put_value(value)

# wrapper - put high/low to bot, if bot not in dict create bot first
def assign_and_create(bot, low, high, min_type, max_type):
    if bot not in Bot.dict_with_bots:
        Bot(bot)
    Bot.dict_with_bots[bot].assign_partners(low, high, min_type, max_type)

# main loop - read data and create all bots
for line in data:
    if line.split()[0] == 'value':
        bot_name = int(line.split()[-1])
        value = int(line.split()[1])
        put_and_create(bot_name, value)
    elif line.split()[0] == 'bot':
        bot_num = int(line.split()[1])
        type_low = line.split()[5]
        bot_low = int(line.split()[6])
        type_high = line.split()[-2]
        bot_high = int(line.split()[-1])
        assign_and_create(bot_num, bot_low, bot_high, type_low, type_high)

