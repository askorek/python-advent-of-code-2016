from time import time

with open("day_09.txt") as f:
    data = f.read()

def decode_string(data):
    decoded = ""
    i = 0
    while i < len(data):
        letter = data[i]
        if letter != '(':
            decoded += letter
            i += 1
        elif letter == '(':
            between_parenthasis = ""
            i += 1
            while data[i] != ')':
                between_parenthasis += data[i]
                i += 1
            how_many_letters = int(between_parenthasis.split('x')[0])
            how_many_times = int(between_parenthasis.split('x')[1])
            letters_to_repeat = data[i + 1: i + 1 + how_many_letters]
            decoded += how_many_times * letters_to_repeat
            i = i + 1 + how_many_letters
    return decoded

decoded = decode_string(data)
print(len(decoded))

# --------------------------- part 2 -------------------

def find_len(data):
    decoded_len = 0
    if '(' not in data:
        return len(data)
    else:
        i = 0
        while i < len(data):
            if data[i] != '(':
                decoded_len += 1
                i += 1
            else:
                between_parenthasis = ""
                i += 1
                while data[i] != ')':
                    between_parenthasis += data[i]
                    i += 1
                how_many_letters = int(between_parenthasis.split('x')[0])
                how_many_times = int(between_parenthasis.split('x')[1])
                letters_to_repeat = data[i + 1: i + 1 + how_many_letters]
                decoded_len += how_many_times * find_len(letters_to_repeat)
                i = i + 1 + how_many_letters
    return decoded_len

a = find_len(data)
print(a)