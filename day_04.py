with open("day_04.txt") as f:
    data = f.readlines()

#data = ['aaaaa-bbb-z-y-x-123[abxyz]/n']

def extract(line):
    line.replace('-','')
    checksum = line[-7:-2]
    sector = line[-11:-8]
    letters = line[:-12]
    return letters, sector, checksum

shift = lambda letter, move: chr((ord(letter) - 97 + move)%26 + 97)

sum_of_sectors = 0
for line in data:
    letters, sector, checksum = extract(line)
    set_of_letters = set()
    for letter in letters.replace('-',''):
        set_of_letters.add(letter)
    letters_amount = {}
    for letter in set_of_letters:
        letters_amount[letter] = letters.count(letter)
    sorted_letters = [v[0] for v in sorted(letters_amount.items(), key=lambda kv: (-kv[1], kv[0]))]
    if ''.join(sorted_letters[:5]) == checksum:
        sum_of_sectors += int(sector)
    shifted = ''
    for letter in letters:
        shifted += shift(letter, int(sector))
    if 'pole' in shifted:
        print(sector)

print(sum_of_sectors)